const express= require('express');
// devuelve un pbgeto
const app= express();


function logger(req,res,next){
    console.log(`Ruta recivida: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
    next();
}

app.use(express.json());
app.use(logger);
// all es una funcion de expres
app.all('/user',(req,res,next)=>{
    console.log('Por aqui paso ');
    //res.send('termino');
    next();

})

// app.get('/',(req,res)=>{
//     res.send('peticion get ');
// })
app.get('/',(req,res)=>{
    res.json({
        nombre:'Alberto',
        apellido:'Lopez'
    });
})

app.post('/user/:id',(req,res)=>{
    console.log(req.body)//cuerpo de la peticion 
    console.log(req.params)
    res.send('peticion post ');
})
app.put('/user/:id',(req,res)=>{
    console.log(req.body)
    res.send(`User ${req.params.id} a sido actualizado`);
})
app.delete('/user/:userId',(req,res)=>{
    res.send(`User ${req.params.userId} a sido eliminado`);
})



app.listen(3000,()=>{
    console.log('server puerto 3000')
});